package com.regsub;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan("com.regsub.mapper") //扫描的mapper
@SpringBootApplication
@EnableScheduling
public class ThirdDataProcessApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThirdDataProcessApplication.class, args);
	}
}
