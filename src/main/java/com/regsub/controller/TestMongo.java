package com.regsub.controller;

import com.regsub.repositories.mongo.TestDataResultInfo;
import com.regsub.repositories.mongo.TestDataInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: demo
 * @description: test
 * @author: wq
 * @create: 2021-03-24 15:07
 **/

@RestController
@RequestMapping("/test")
public class TestMongo {

    @Autowired
    TestDataInfoRepository testDataInfoRepository;

    @RequestMapping("mongo")
    public String sendMail(){
        TestDataResultInfo info = new TestDataResultInfo();
        info.setCreateTime("test");
        info.setGlobalId("aaa");
        info.setProcesstime(222);
        info.setRefusedata("wer");
        testDataInfoRepository.save(info);
        return "success";
    }

}
