package com.regsub.controller;

import com.regsub.config.redis.RedisService;
import com.regsub.repositories.mongo.TestDataResultInfo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: demo
 * @description: test redis
 * @author: wq
 * @create: 2021-03-25 17:58
 **/

@RestController
@RequestMapping("/test")
public class TestRedis {

    @Autowired
    RedisService redisService;

    @RequestMapping("redis")
    public String sendMail(){
        TestDataResultInfo info = new TestDataResultInfo();
        info.setCreateTime("test");
        info.setGlobalId("aaa");
        info.setProcesstime(222);
        info.setRefusedata("wer");
        redisService.set("aaa","111");

        Student student = new Student();
        student.setCode("11");
        student.setName("22");
        redisService.set("sss",student);
        return "success";
    }

    @RequestMapping("gredis")
    public String getRedis() {
//        return redisService.get("aaa").toString();
        return redisService.get("sss",Student.class).getName();
    }

    @Data
    class Student {
        private String name;
        private String code;
    }
}
