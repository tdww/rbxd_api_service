package com.regsub.exception;

import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * @program: 监管报送
 * @description: 异常类
 * @author: wq
 * @create: 2020-12-30 15:49
 **/


public class BusinessException extends RuntimeException {

    private int code = HttpStatus.INTERNAL_SERVER_ERROR.value();

    private String source;
    private Date timestamp = new Date();
    private int status;
    private String error;
    private String exception;
    private String path;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(final int code, String message) {
        super(message);
        this.setCode(code);
    }
}
