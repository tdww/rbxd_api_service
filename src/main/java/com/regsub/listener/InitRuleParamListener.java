package com.regsub.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashMap;

/**
 * @program: rc-thirddata-process
 * @description: 初始化变量
 * @author: wq
 * @create: 2021-04-08 17:18
 **/


public class InitRuleParamListener implements ApplicationListener<ApplicationReadyEvent> {
    private Logger logger = LoggerFactory.getLogger(InitRuleParamListener.class);

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        if (event.getApplicationContext().getParent().getParent() == null) {
            logger.debug("----------------begin to init param----------------");
            ConfigurableApplicationContext applicationContext = event.getApplicationContext();
            //解决newRuleEngineInitManage一直为空
//            newRuleEngineInitManage = applicationContext.getBean(NewRuleEngineInitManage.class);
//
//
//            //初始化配置数据
//            CommonConstants.CHANNEL_ATTR_DATA_TYPE = new HashMap();
//            CommonConstants.DATA_APP_PACKAGE = new HashMap<>();
//            CommonConstants.DATA_PACKAGE_WARNSCORE = new HashMap<>();
//            CommonConstants.DATA_PACKAGE_RULE_SET = new HashMap<>();
//            CommonConstants.DATA_PACKAGE_RULESET_RULE = new HashMap<>();
//            CommonConstants.DATA_APP_INTERFACE_CONFIG = new HashMap<>();
//
//            newRuleEngineInitManage.initParam("");
            logger.debug("----------------init param end----------------");
        }
    }
}
