package com.regsub.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegSubMapper {


    int CONTRACT_INFO_CNT(@Param("contractNo") String contractNo,@Param("sdate") String sdate, @Param("edate") String edate);
    int ISSUE_INFO_CNT(@Param("contractNo") String contractNo,@Param("sdate") String sdate, @Param("edate") String edate);
    int REPAY_INFO_CNT(@Param("contractNo") String contractNo,@Param("sdate") String sdate, @Param("edate") String edate);
    int PAYPLAN_INFO_CNT(@Param("contractNo") String contractNo,@Param("sdate") String sdate, @Param("edate") String edate);


}
