package com.regsub.mapper;

import com.regsub.repositories.mysql.TdsApi;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Classname TdsApiMapper
 * @Description 外部api信息描述
 * @Date 2021/4/12 9:10
 * @Created by gyh
 */
@Mapper
public interface TdsApiMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(TdsApi record);

    int insertOrUpdate(TdsApi record);

    int insertOrUpdateSelective(TdsApi record);

    int insertSelective(TdsApi record);

    TdsApi selectByPrimaryKey(Integer id);

    List<TdsApi> selectAll();

    int updateByPrimaryKeySelective(TdsApi record);

    int updateByPrimaryKey(TdsApi record);

    int updateBatch(List<TdsApi> list);

    int updateBatchSelective(List<TdsApi> list);

    int batchInsert(@Param("list") List<TdsApi> list);

}
