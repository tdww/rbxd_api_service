package com.regsub.mapper;

import com.regsub.repositories.mysql.TdsUserApi;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Classname TdsUserApiMapper
 * @Description 数据服务用户信息
 * @Date 2021/4/12 9:15
 * @Created by gyh
 */
@Mapper
public interface TdsUserApiMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TdsUserApi record);

    int insertOrUpdate(TdsUserApi record);

    int insertOrUpdateSelective(TdsUserApi record);

    int insertSelective(TdsUserApi record);

    TdsUserApi selectByPrimaryKey(Integer id);

    List<TdsUserApi> selectAll();

    int updateByPrimaryKeySelective(TdsUserApi record);

    int updateByPrimaryKey(TdsUserApi record);

    int updateBatch(List<TdsUserApi> list);

    int updateBatchSelective(List<TdsUserApi> list);

    int batchInsert(@Param("list") List<TdsUserApi> list);

}
