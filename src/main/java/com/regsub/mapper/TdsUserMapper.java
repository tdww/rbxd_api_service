package com.regsub.mapper;

import com.regsub.repositories.mysql.TdsUser;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Classname TdsUserMapper
 * @Description 用户-api相关信息
 * @Date 2021/4/12 9:15
 * @Created by gyh
 */
@Mapper
public interface TdsUserMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(TdsUser record);

    int insertOrUpdate(TdsUser record);

    int insertOrUpdateSelective(TdsUser record);

    int insertSelective(TdsUser record);

    TdsUser selectByPrimaryKey(Integer id);

    List<TdsUser> selectAll();

    int updateByPrimaryKeySelective(TdsUser record);

    int updateByPrimaryKey(TdsUser record);

    int updateBatch(List<TdsUser> list);

    int updateBatchSelective(List<TdsUser> list);

    int batchInsert(@Param("list") List<TdsUser> list);

}
