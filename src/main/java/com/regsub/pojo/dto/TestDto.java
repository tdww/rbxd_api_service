package com.regsub.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @Classname TestDto
 * @Description 测试参数
 * @Date 2021/4/12 11:25
 * @Created by gyh
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestDto {

    @NotBlank(message = "appKey不能为空")
    private String appkey;

    private String name;

    private String idcard;

    private String mobile;

    @NotBlank(message = "签名不能为空")
    private String sign;
}
