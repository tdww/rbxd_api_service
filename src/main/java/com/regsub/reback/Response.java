package com.regsub.reback;

import org.springframework.http.HttpStatus;

/**
 * @program: demo
 * @description: test feedback
 * @author: wq
 * @create: 2021-03-16 10:28
 **/


public class Response {
    public static SuccessModel success(final Object data) {
        SuccessModel sm = new SuccessModel();
        sm.setCode(HttpStatus.OK.value());
        sm.setData(data);
        return sm;
    }

    public static SuccessModel success(final Object data, final String message) {
        SuccessModel sm = new SuccessModel();
        sm.setCode(HttpStatus.OK.value());
        sm.setMessage(message);
        sm.setData(data);
        return sm;
    }
}
