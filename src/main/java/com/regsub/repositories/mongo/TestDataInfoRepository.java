package com.regsub.repositories.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author: create by wq
 * @version: v1.0
 * @description: rule-engine-data-process
 * @date:2019/11/1
 **/
public interface TestDataInfoRepository extends MongoRepository<TestDataResultInfo, Long> {
}
