package com.regsub.repositories.mongo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @program: rule-engine-data-process
 * @description: 规则包测试结果保存
 * @author: wq
 * @create: 2019-11-01 18:15
 **/

@Data
@Document(collection = "sys.third.data")
public class TestDataResultInfo {
    private String globalId;
    private long processtime;
    private Object refusedata;
    private String createTime;

}
