package com.regsub.repositories.mysql;

import com.regsub.annotation.GeneratedValue;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

public class CarrierReportResults implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private String appkey;
    private Long customerId;
    private String orderNum;
    private String resultData;
    private Date createTime;
    private String globalId;
    private String type;
    private String dataDesc;
    private String fullPath;
    private String paramMd5; //入参加密
    private String dataFrom; //new,history
    private String ifFail;//接口调用是否异常
    private String ifPay; //是否计费

    public String getIfPay() {
        return ifPay;
    }

    public void setIfPay(String ifPay) {
        this.ifPay = ifPay;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public String getDataDesc() {
        return dataDesc;
    }

    public void setDataDesc(String dataDesc) {
        this.dataDesc = dataDesc;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getGlobalId() {
        return globalId;
    }

    public void setGlobalId(String globalId) {
        this.globalId = globalId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParamMd5() {
        return paramMd5;
    }

    public void setParamMd5(String paramMd5) {
        this.paramMd5 = paramMd5;
    }

    public String getDataFrom() {
        return dataFrom;
    }

    public void setDataFrom(String dataFrom) {
        this.dataFrom = dataFrom;
    }

    public String getIfFail() {
        return ifFail;
    }

    public void setIfFail(String ifFail) {
        this.ifFail = ifFail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarrierReportResults)) return false;

        CarrierReportResults that = (CarrierReportResults) o;

        if (!id.equals(that.id)) return false;
        if (!appkey.equals(that.appkey)) return false;
        if (!orderNum.equals(that.orderNum)) return false;
        if (!resultData.equals(that.resultData)) return false;
        if (!createTime.equals(that.createTime)) return false;
        if (!globalId.equals(that.globalId)) return false;
        return type.equals(that.type);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + appkey.hashCode();
        result = 31 * result + orderNum.hashCode();
        result = 31 * result + resultData.hashCode();
        result = 31 * result + createTime.hashCode();
        result = 31 * result + globalId.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CarrierReportResults{" +
                "id=" + id +
                ", appkey='" + appkey + '\'' +
                ", customerId=" + customerId +
                ", orderNum='" + orderNum + '\'' +
                ", resultData='" + resultData + '\'' +
                ", createTime=" + createTime +
                ", globalId='" + globalId + '\'' +
                ", type='" + type + '\'' +
                ", dataDesc='" + dataDesc + '\'' +
                ", fullPath='" + fullPath + '\'' +
                ", paramMd5='" + paramMd5 + '\'' +
                ", dataFrom='" + dataFrom + '\'' +
                ", ifFail='" + ifFail + '\'' +
                ", ifPay='" + ifPay + '\'' +
                '}';
    }
}
