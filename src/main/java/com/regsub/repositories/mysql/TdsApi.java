package com.regsub.repositories.mysql;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Classname TdsApi
 * @Description 外部api信息描述
 * @Date 2021/4/12 9:10
 * @Created by gyh
 */
@ApiModel(value = "外部api信息描述")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdsApi implements Serializable {

    @ApiModelProperty(value = "主键")
    private Integer id;

    /**
     * 接口唯一标识
     */
    @ApiModelProperty(value = "接口唯一标识")
    private String apiType;

    /**
     * 接口中文描述
     */
    @ApiModelProperty(value = "接口中文描述")
    private String apiName;

    /**
     * 处理类唯一标识，对应程序处理类
     */
    @ApiModelProperty(value = "处理类唯一标识，对应程序处理类")
    private String optType;

    /**
     * 参数映射
     */
    @ApiModelProperty(value = "参数映射")
    private String params;

    /**
     * 访问地址
     */
    @ApiModelProperty(value = "访问地址")
    private String addr;

    /**
     * 是否开启，1-是；0-否
     */
    @ApiModelProperty(value = "是否开启，1-是；0-否")
    private Integer isOpen;

    /**
     * 计费方式，1-查得计费；2-查询计费
     */
    @ApiModelProperty(value = "计费方式，1-查得计费；2-查询计费")
    private Integer calType;

    /**
     * 单价（单位：元）
     */
    @ApiModelProperty(value = "单价（单位：元）")
    private BigDecimal price;

    /**
     * 添加时间
     */
    @ApiModelProperty(value = "添加时间")
    private Date createTime;

    /**
     * 添加人
     */
    @ApiModelProperty(value = "添加人")
    private String createUser;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人")
    private String modifyUser;

    private static final long serialVersionUID = 1L;
}
