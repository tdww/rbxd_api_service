package com.regsub.repositories.mysql;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname TdsUser
 * @Description 数据服务用户信息
 * @Date 2021/4/12 9:15
 * @Created by gyh
 */
@ApiModel(value = "数据服务用户信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdsUser implements Serializable {

    @ApiModelProperty(value = "主键")
    private Integer id;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String userName;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 是否开启
     */
    @ApiModelProperty(value = "是否开启")
    private Integer isOpen;

    /**
     * 公司名称
     */
    @ApiModelProperty(value = "公司名称")
    private String compName;

    /**
     * 唯一编码
     */
    @ApiModelProperty(value = "唯一编码")
    private String appKey;

    /**
     * 加密编码
     */
    @ApiModelProperty(value = "加密编码")
    private String secretKey;

    /**
     * 添加时间
     */
    @ApiModelProperty(value = "添加时间")
    private Date createTime;

    /**
     * 添加人
     */
    @ApiModelProperty(value = "添加人")
    private String createUser;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人")
    private String modifyUser;

    private static final long serialVersionUID = 1L;
}
