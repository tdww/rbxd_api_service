package com.regsub.repositories.mysql;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname TdsUserApi
 * @Description 用户-api相关信息
 * @Date 2021/4/12 9:15
 * @Created by gyh
 */
@ApiModel(value="用户-api相关信息")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdsUserApi implements Serializable {

    @ApiModelProperty(value="主键")
    private Integer id;

    /**
    * 用户唯一编码
    */
    @ApiModelProperty(value="用户唯一编码")
    private String appKey;

    /**
    * 数据类型
    */
    @ApiModelProperty(value="数据类型")
    private String apiType;

    /**
    * 是否开启，1-是；0-否
    */
    @ApiModelProperty(value="是否开启，1-是；0-否")
    private Integer isOpen;

    /**
    * 计费方式，1-查得计费；2-查询计费
    */
    @ApiModelProperty(value="计费方式，1-查得计费；2-查询计费")
    private Integer calType;

    /**
    * 单价（单位：元）
    */
    @ApiModelProperty(value="单价（单位：元）")
    private BigDecimal price;

    /**
    * 缓存时长（单位：秒，默认为0）
    */
    @ApiModelProperty(value="缓存时长（单位：秒，默认为0）")
    private Integer cacheDuration;

    /**
    * 添加时间
    */
    @ApiModelProperty(value="添加时间")
    private Date createTime;

    /**
    * 添加人
    */
    @ApiModelProperty(value="添加人")
    private String createUser;

    /**
    * 修改时间
    */
    @ApiModelProperty(value="修改时间")
    private Date modifyTime;

    /**
    * 修改人
    */
    @ApiModelProperty(value="修改人")
    private String modifyUser;

    private static final long serialVersionUID = 1L;
}
