package com.regsub.service.thirddata;

import com.regsub.service.thirddata.option.ThirdDataOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: rc-data-process
 * @description: 三方数据工厂
 * @author: wq
 * @create: 2020-05-15 09:54
 **/

@Service
public class ThirdDataFactory {

    @Autowired
    Map<String, ThirdDataOption> thirdDataOpts = new ConcurrentHashMap<>();

    public ThirdDataOption getThirdDataOpt(String component) throws Exception{
        ThirdDataOption thirdDataOpt = thirdDataOpts.get(component);
        if(thirdDataOpt == null) {
            throw new RuntimeException("no thirdDataOpt defined");
        }
        return thirdDataOpt;
    }
}
