package com.regsub.service.thirddata;

import com.regsub.repositories.mysql.DataInterfaceConfig;
import com.regsub.service.thirddata.option.ThirdDataOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @program: rc-thirddata-process
 * @description: 三方数据处理
 * @author: wq
 * @create: 2021-04-09 09:28
 **/

@Component
public class ThirdDataService {
    @Autowired
    private ThirdDataFactory thirdDataFactory;

    public Object thirdDataOpt(DataInterfaceConfig dataInertfaceConfig, Map<String, String> map) {
        try {
            if (thirdDataFactory.getThirdDataOpt(dataInertfaceConfig.getHost()) != null) {
                ThirdDataOption dataOption = thirdDataFactory.getThirdDataOpt(dataInertfaceConfig.getHost());
                Object result = dataOption.executor(dataInertfaceConfig, map, carrierReportResults);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
