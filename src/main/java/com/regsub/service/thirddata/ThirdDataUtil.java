package com.regsub.service.thirddata;

import com.regsub.repositories.mysql.DataInterfaceConfig;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: rule-engine-data-process
 * @description: 三方数据工具类
 * @author: wq
 * @create: 2020-06-04 17:44
 **/


public class ThirdDataUtil {

    /**
     * @Description: 参数匹配
     * @Param: [dataInertfaceConfig, map, dataType] 
     * @return: java.util.Map<java.lang.String,java.lang.String> 
     * @Author: wq 
     * @Date: 2020/6/15 2:02 PM
    **/ 
    public static Map<String,String> paramsProcess(DataInterfaceConfig dataInertfaceConfig, Map<String, String> map, String optType) {
        Map<String, String> mapValue = new HashMap<>();

        if (StringUtils.equals(ThirdDataManage.DATA_TYPE_TEST, optType)) {
            for (String keys : map.keySet()) {
                mapValue.put(keys, map.get(keys));
            }
        } else {
            String[] mediaTypeTwo = dataInertfaceConfig.getMediaTypeTwo().split(",");
            for (String infoTwo : mediaTypeTwo) {
                String[] infos = infoTwo.split("-");
                if (infos.length == 2) {
                    if (StringUtils.isNotBlank(map.get(infos[1]))) {
                        mapValue.put(infos[0], map.get(infos[1]));
                    }
                } else if (infos.length == 3) {
                    if ("m".equals(infos[2])) {
                        mapValue.put(infos[0], infos[1]);
                    } else if ("n".equals(infos[2])) {
                        mapValue.put(infos[0], map.get(infos[1]));
                    }
                }
                else if (infos.length == 1){
                    mapValue.put(infos[0], map.get(infos[0]));
                }
            }
            if (mapValue.size() != mediaTypeTwo.length) {
                throw new RuntimeException("订单-" + map.get("orderNum") + "-调用数据项-"+dataInertfaceConfig.getConfigName()+"-参数异常");
            }
        }
        return mapValue;
    }
}
