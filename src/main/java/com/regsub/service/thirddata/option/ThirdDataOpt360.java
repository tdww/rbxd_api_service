package com.regsub.service.thirddata.option;

import com.regsub.remote.RestTemplateUtil;
import com.regsub.repositories.mysql.CarrierReportResults;
import com.regsub.repositories.mysql.DataInterfaceConfig;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @program: rule-engine-data-process
 * @description: 360数据调用
 * @author: wq
 * @create: 2020-10-22 15:36
 **/

@Component("360data")
public class ThirdDataOpt360 extends ThirdDataOption{

    @Value("${thirddata.api360.privateKeyPath}")
    private String privateKeyPath;

    @Value("${thirddata.api360.appid}}")
    private String appid;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Override
    public String executor(DataInterfaceConfig dataInterfaceConfig, Map<String, String> map, CarrierReportResults carrierReportResults) {
        try {
            HashMap<String, Object> requstMap = new HashMap<>();
            requstMap.put("method",map.get("method"));
            requstMap.put("sign_type","RSA");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("phone",DigestUtils.md5DigestAsHex(map.get("phone").getBytes()));

            if ("tianji.api.agentn.scorev21".equals(map.get("method"))) {
                jsonObject.put("idfa","");
                jsonObject.put("imei","");
            } else {
                jsonObject.put("name",DigestUtils.md5DigestAsHex(map.get("name").getBytes()));
                jsonObject.put("idNumber",DigestUtils.md5DigestAsHex(map.get("idNumber").getBytes()));
            }
            jsonObject.put("isMd5",1);
            requstMap.put("biz_data",jsonObject);
            requstMap.put("app_id", appid);
            requstMap.put("version",map.get("version"));
            requstMap.put("format","json");
            requstMap.put("timestamp",DateUtils.getCurrentTimeLong());

            byte[] bytes = RsaUtil.generateSHA1withRSASigature(getSign(requstMap), RsaUtil.getPrivateKey(privateKeyPath), "utf-8");
            String sign = Base64Utils.encode(bytes);

            requstMap.put("sign", sign);

            LOGGER.debug("请求地址为-{}-请求参数-{}", dataInterfaceConfig.getAddr(),getSign(requstMap));

            String result = restTemplateUtil.post(dataInterfaceConfig.getAddr(), requstMap, String.class);

            JSONObject data = JSONObject.fromObject(result);
            if (data.has("error") && data.getString("error") != null &&
                    ("200".equals(data.getString("error")) ||
                            ("tianji.api.agentn.scorev21".equals(map.get("method"))
                                    && "90001108,90001109,90001522,90001524".contains(data.getString("error"))))) {
                carrierReportResults.setIfFail(ThirdDataManage.DATA_SUCCESS);
                if(data.has(map.get("method").replace(".","_").concat("_response")) &&
                        !data.getJSONObject(map.get("method").replace(".","_").concat("_response")).isEmpty()) {
                    carrierReportResults.setIfPay(ThirdDataManage.DATA_PAY);
                }
                return result;
            }
            carrierReportResults.setIfFail(ThirdDataManage.DATA_FAIL);
            return result;
        } catch (Exception e) {
            LOGGER.error("获取数据异常-{}", e.getMessage());
        }
        carrierReportResults.setIfFail(ThirdDataManage.DATA_FAIL);
        return "error";
    }

    /**
     * 签名拼接函数
     * @param param 参数 key-value 对
     * @return
     */
    private String getSign(HashMap<String, Object> param){
        Map<String,Object> result = new LinkedHashMap<String, Object>();
        param.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).forEachOrdered(x -> result.put(x.getKey(), x.getValue()));
        StringBuilder signStringBuilder = new StringBuilder();
        for (Map.Entry entry: result.entrySet()) {
            signStringBuilder.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        String signStr = signStringBuilder.substring(1);
        return signStr;
    }
}
