package com.regsub.service.thirddata.option;

import com.regsub.repositories.mysql.CarrierReportResults;
import com.regsub.repositories.mysql.DataInterfaceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @program: rc-data-process
 * @description: 三方数据
 * @author: wq
 * @create: 2020-05-15 10:03
 **/


public abstract class ThirdDataOption {
    public Logger LOGGER = LoggerFactory.getLogger(getClass());

    public abstract String executor(DataInterfaceConfig dataInterfaceConfig, Map<String, String> map, CarrierReportResults carrierReportResults);

}
