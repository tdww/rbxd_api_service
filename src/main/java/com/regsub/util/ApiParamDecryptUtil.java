package com.regsub.util;

import sun.misc.BASE64Decoder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gyh
 * @Classname ApiParamDecryptUtil
 * @Description 接收参数解密工具类
 * @Date 2021/4/12 13:11
 */
public class ApiParamDecryptUtil {

    public static final String SIGN = "sign";

    public static final String APP_KEY = "appkey";

    public static Map<String, String> paramMapDecrypt(Map<String, String> paramMap) throws IOException {
        // 判断签名
        List<String> tempList = new ArrayList<>();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            if (isNtoKeyOrSign(entry.getKey())) {
                tempList.add(entry.getKey() + "=" + entry.getValue());
            }
        }
        String paramString = String.join("&", tempList);
        String sign = EncryptUtil.md5(paramString);
        // todo 签名不一致，做处理
        if (!sign.equals(paramMap.get(SIGN))) {

        }

        Map<String, String> result = new HashMap<>();
        BASE64Decoder decoder = new BASE64Decoder();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            if (isNtoKeyOrSign(entry.getKey())) {
                byte[] bytes = decoder.decodeBuffer(entry.getValue());
                result.put(entry.getKey(), new String(bytes, StandardCharsets.UTF_8));
            }
        }
        return result;
    }

    /**
     * 判断字段是否是appkey或sign
     *
     * @param str
     * @return
     */
    private static boolean isNtoKeyOrSign(String str) {
        return !SIGN.equals(str) && !APP_KEY.equals(str);
    }

}
