package com.regsub.util;

import com.regsub.mapper.TdsApiMapper;
import com.regsub.mapper.TdsUserApiMapper;
import com.regsub.mapper.TdsUserMapper;
import com.regsub.repositories.mysql.TdsApi;
import com.regsub.repositories.mysql.TdsUser;
import com.regsub.repositories.mysql.TdsUserApi;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: rc-thirddata-process
 * @description: 公共静态变量
 * @author: wq
 * @create: 2021-04-08 17:17
 **/


public class CommonConstants {

    /**
     * 用户唯一编码 - 用户信息
     * app_key - TdsUser
     */
    public static Map<String, TdsUser> userMap = new HashMap<>();

    /**
     * 用户唯一编码 - Api唯一编码 - 关系表信息
     * app_key - api_type - TdsUserApi
     */
    public static Map<String, Map<String, TdsUserApi>> userApiMap = new HashMap<>();

    /**
     * Api唯一标识-api信息
     * api_type - TdsApi
     */
    public static Map<String, TdsApi> apiMap = new HashMap<>();


    @Autowired
    private TdsUserMapper tdsUserMapper;

    @Autowired
    private TdsUserApiMapper tdsUserApiMapper;

    @Autowired
    private TdsApiMapper tdsApiMapper;

    /**
     * 初始化缓存数据
     */
    @PostConstruct
    public void init() {
        refreshUserMap();
        refreshUserApiMap();
        refreshApiMap();
    }

    /**
     * 刷新用户信息缓存
     */
    public void refreshUserMap() {
        List<TdsUser> userList = tdsUserMapper.selectAll();
        userMap = userList.stream().collect(Collectors.toMap(TdsUser::getAppKey, user -> user));
    }

    /**
     * 刷新用户-api关系信息缓存
     */
    public void refreshUserApiMap() {
        List<TdsUserApi> userApiList = tdsUserApiMapper.selectAll();
        Map<String, List<TdsUserApi>> userApiListMap = userApiList.stream().collect(Collectors.groupingBy(TdsUserApi::getAppKey));
        userApiMap = new HashMap<>();
        for (Map.Entry<String, List<TdsUserApi>> entry : userApiListMap.entrySet()) {
            Map<String, TdsUserApi> tempMap = entry.getValue().stream().collect(Collectors.toMap(TdsUserApi::getApiType, userApi -> userApi));
            userApiMap.put(entry.getKey(), tempMap);
        }
    }

    /**
     * 刷新新api信息缓存
     */
    public void refreshApiMap() {
        List<TdsApi> apiList = tdsApiMapper.selectAll();
        apiMap = apiList.stream().collect(Collectors.toMap(TdsApi::getApiType, api -> api));
    }
}
