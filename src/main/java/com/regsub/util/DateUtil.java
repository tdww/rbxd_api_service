package com.regsub.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @program: 监管报送
 * @description: 日期工具类
 * @author: wq
 * @create: 2020-09-22 15:41
 **/


public class DateUtil {
    public static final String DATE_FORMATE = "yyyyMMdd";
    public static final String DATE_FORMATE_2 = "yyyy-MM-dd";
    public static final String TIME_FORMATE = "HHmmssSSS";//

    public static String getNowDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMATE);
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    public static String getNowTime() {
        SimpleDateFormat formatter = new SimpleDateFormat(TIME_FORMATE);
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    public static String getFormateDate(String sdate) {
        SimpleDateFormat sdf1 = new SimpleDateFormat(DATE_FORMATE) ;
        SimpleDateFormat sdf2 = new SimpleDateFormat(DATE_FORMATE_2) ;
        Date d = null ;
        try{
            d = sdf2.parse(sdate) ;
        }catch(Exception e){
            e.printStackTrace() ;
        }
        return sdf1.format(d);
    }

    public static String getDaysAway(int days) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMATE);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, - days);
        Date d = c.getTime();
        return sdf.format(d);
    }

    public static void main(String[] args) {

        System.out.println(DateUtil.getDaysAway(1));
        System.out.println(DateUtil.getNowDate());
    }
}
