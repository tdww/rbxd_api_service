package com.regsub.util;


import org.apache.shiro.crypto.hash.SimpleHash;

/**
 * 加解密工具类
 *
 * @author gyh
 * @date 2021/4/12
 */
public class EncryptUtil {

    /**
     * 默认加密次数
     */
    public static final Integer DEFAULT_ITERATIONS = 1;

    public static String md5(String paramString) {
        String algorithmName = "MD5";
        return new SimpleHash(algorithmName, paramString, DEFAULT_ITERATIONS).toHex();
    }

    public static void main(String[] args) {
    }
}
