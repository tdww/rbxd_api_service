package com.regsub.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import java.util.Map;

/**
 * @program: 监管报送
 * @description: list操作
 * @author: wq
 * @create: 2020-09-27 13:52
 **/


public class ListOptUtil {

    protected static Logger LOGGER = LoggerFactory.getLogger(ListOptUtil.class);

    public static void listBeanToMap(List<?> result, List<Map<String,Object>> mapList) {

        for(Object obj : result) {
            Map<String, Object> map;
            try {
                map = new ObjectMapper().convertValue(obj, Map.class);
                mapList.add(map);
            } catch (Exception e) {
                LOGGER.error("mapToBean 数据转换异常!" + e.getMessage());
            }
        }
    }

    public static void main(String[] args) {
////        List<CONTRACT_INFO> data = new ArrayList<>();
////
////        CONTRACT_INFO contract_info = new CONTRACT_INFO();
////        contract_info.setCUSTOMER_NAME("aaaa");
////
////        List<CO_CUSTOMER_INFO> temp = new ArrayList<>();
////        CO_CUSTOMER_INFO co_customer_info = new CO_CUSTOMER_INFO();
////        co_customer_info.setCERTIFICATE_NO("test");
////        co_customer_info.setCUSTOMER_NAME("zhangsan");
////        co_customer_info.setTELEPHONE("1213");
////
////        temp.add(co_customer_info);
////
////        contract_info.setCO_CUSTOMER_INFO(temp);
//
//        data.add(contract_info);
//        List<Map<String,Object>> mapList = new ArrayList<>();
//
//        listBeanToMap(data, mapList);
//
//        System.out.println(mapList.toString());


    }
}
