package com.regsub.util;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @program: 监管报送
 * @description: 将深度嵌套的map对象转换为扁平化的map对象输出
 * @author: wq
 * @create: 2021-03-03 09:22
 **/


public class MapFlatUtil {

    private static final String SEPARATOR = "_";

    /**
     * 深度嵌套map对象转大map（扁平化）
     * @param source 源map
     * @param parentNode 父节点扁平化之后的名字
     * @return map
     */
    public static Map<String, Object> flat(Map<String, Object> source, String parentNode) {
        Map<String, Object> flat = new HashMap<>();
        Set<Map.Entry<String, Object>> set  = source.entrySet();
        String prefix = StringUtils.isNotBlank(parentNode) ? parentNode + SEPARATOR : "";
        set.forEach(entity -> {
            Object value = entity.getValue();
            String key = entity.getKey();
            String newKey = prefix + key;
            if (value instanceof Map) {
                flat.putAll(flat((Map)value, newKey));
            } else {
                flat.put(newKey, value);
            }
        });
        return flat;
    }
    public static void main (String[] args) {
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> map2 = new HashMap<>();
        Map<String, Object> map3 = new HashMap<>();
        map.put("root", "root");
        map2.put("root", "root");
        map2.put("map3", map3);
        map3.put("root", "root");
        map.put("map2", map2);

        System.out.println(flat(map, null));
    }
}