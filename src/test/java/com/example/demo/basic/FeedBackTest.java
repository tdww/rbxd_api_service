package com.regsub.demo.basic;

import com.regsub.service.jobDetail.enums.DataType;
import com.regsub.util.*;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: demo
 * @description: 测试结果处理
 * @author: wq
 * @create: 2021-02-04 11:29
 **/


public class FeedBackTest {

    public static void resultOpt() {
        String result = "00000647<?xml version=\"1.0\" encoding=\"UTF-8\"?><transaction><header><msg><RET><RET_MSG>交易成功。</RET_MSG><RET_CODE>000000</RET_CODE></RET><TRAN_TIMESTAMP>080322402</TRAN_TIMESTAMP><MESSAGE_TYPE>1220</MESSAGE_TYPE><SOURCE_BRANCH_NO>null</SOURCE_BRANCH_NO><BRANCH_ID>91500114MA5YNMRP9N</BRANCH_ID><MESSAGE_CODE>0199</MESSAGE_CODE><RET_STATUS>S</RET_STATUS><SERVICE_CODE>SVR_PTLN</SERVICE_CODE><DEST_BRANCH_NO>null</DEST_BRANCH_NO><TRAN_DATE>20210302</TRAN_DATE><SEQ_NO>8054893345215258624</SEQ_NO></msg></header><body><RtrTx><MSG_CODE>200000</MSG_CODE><MSG_INFO>成功</MSG_INFO><BATCH_NO>20210224.1614245480.85</BATCH_NO></RtrTx></body></transaction>";
        String presult = result.substring(8,result.length());
        System.out.println(presult);

        Map<String, Object> map = XMLUtil.createMapByXml(presult);
        System.out.println(map);
//        System.out.println(map.get("transaction"));
        Map<String, Object> flatmap = MapFlatUtil.flat(map,null);
        System.out.println(flatmap.get("transaction_body_RtrTx_MSG_CODE"));


        System.out.println(MapFlatUtil.flat(map,null));
    }
    public static void main(String[] args) {

//        resultOpt();

        String host = "113.200.27.104";
        int port = 8074;
        String filePath = "/Users/huajuan/program/study/reg_sub_file_str/test_dir/20210318";
        String fileName = "91500114MA5YNMRP9N20210224CONTRACT_INFO10.xml";

        String optResult = "处理成功";
        File dir = new File(filePath);

        if(dir.isDirectory()){
            String[] list = dir.list();
            for (String str : list) {
                if (!str.contains("xml")) {
                    continue;
                }
                System.out.println("----------------------------------");
                System.out.println(str);
                String fp = makeResult(filePath,str);

                String result = ScoketNotifyUtil.notifyData(fp,host,port);
                System.out.println(result);
                if (result.contains("存在失败")) {
                    optResult = "存在失败";
                }
            }
        }





//        String pt = makeResult(filePath,fileName);
//
//        String result = ScoketNotifyUtil.notifyData(pt,host,port);
//        System.out.println(result);
    }

    public static String makeResult(String path, String fileName) {

        try {
            Map<String, String> msgMap = new HashMap<>();
            msgMap.put("SERVICE_CODE","SVR_PTLN");
            msgMap.put("TRAN_CODE","PTLN199");
            msgMap.put("TRAN_MODE","ASYNC");
            msgMap.put("BRANCH_ID","91500114MA5YNMRP9N");
            msgMap.put("TRAN_DATE",DateUtil.getNowDate());
            msgMap.put("TRAN_TIMESTAMP",DateUtil.getNowTime());
            msgMap.put("USER_LANG","CHINESE");
            msgMap.put("SEQ_NO",String.valueOf(SnowFlake.nextId()));
            msgMap.put("MODULE_ID","CL");
            msgMap.put("MESSAGE_TYPE","1220");
            msgMap.put("MESSAGE_CODE","0199");
            msgMap.put("FILE_PATH",fileName);

            Map<String,Object> headerMap = new HashMap<>();
            headerMap.put("msg",msgMap);

            Map<String,Object> transactionMap = new HashMap<>();
            transactionMap.put("header",headerMap);

            Map<String, Object> gettxMap = new HashMap<String, Object>();
            gettxMap.put("FILE_NAME", fileName);
            for (DataType c : DataType.values()) {
                if (fileName.contains(c.getEnName())) {
                    gettxMap.put("DATA_TYPE", c.getEnName());
                    break;
                }
            }

            Map<String,Object> bodyMap = new HashMap<String, Object>();
            bodyMap.put("GetTx",gettxMap);

            transactionMap.put("body",bodyMap);

            Map<String,Object> resultMap = new HashMap<>();
            resultMap.put("transaction",transactionMap);

            StringBuffer sb = new StringBuffer();
            XMLUtil.mapToXML(resultMap,sb);

            String result = sb.insert(0,"<?xml version=\"1.0\" encoding=\"utf-8\"?>").toString();

            String requestDataLength = String.valueOf(result.length());
            String targetDataLength = StringUtils.leftPad(requestDataLength, 8, "0");
            result = sb.insert(0,targetDataLength).toString();

            String filePath = path.concat("/opt_result/").concat(fileName);

            FileUtil.save(result,filePath);
            return filePath;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }
}
