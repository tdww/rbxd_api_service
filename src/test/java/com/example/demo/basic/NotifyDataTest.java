package com.regsub.demo.basic;

import com.regsub.util.ScoketNotifyUtil;

import java.io.File;

/**
 * @program: demo
 * @description: 通知
 * @author: wq
 * @create: 2021-01-09 11:59
 **/


public class NotifyDataTest {
    public static void main(String[] args) {

        try {
            String strFilePath = "/Users/huajuan/program/study/reg_sub_file_str/notify";
            String host = "113.200.27.105";
            int port = 7074;

            File[] list = new File(strFilePath).listFiles();

            for(File file : list)
            {
                if(file.isFile())
                {
                    if (file.getName().endsWith("xml")) {
                        String filePath = file.getAbsolutePath();
                        String result = ScoketNotifyUtil.notifyData(filePath,host,port);
                        System.out.println(result);
                        if (result.isEmpty()) {
                            System.out.println("通知异常，未收到返回结果");
                        } else {
                            System.out.println("通知成功");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
